<?php

namespace App\DataFixtures;

use App\Entity\Heat;
use App\Entity\Property;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        //create 2 Heat
        for ($i = 0; $i < 2; $i++) {
            $heat = new Heat();
            $heat->setName($faker->century);
            $manager->persist($heat);
        }

        $manager->flush();

        $heats = $manager->getRepository(Heat::class)->findAll();
        //create 100 property for each heat
        for ($i = 0; $i < 100; $i++) {
            $heat = $faker->numberBetween(1, 2);
            $heat = $manager->getRepository(Heat::class)->find($heat);
            $property = new Property();
            $property
                ->setTitle($faker->word)
                ->addHeat($faker->randomElement($heats))
                ->setBedrooms($faker->numberBetween(1, 10))
                ->setFloor($faker->numberBetween(0, 15))
                ->setPrice($faker->numberBetween(100000, 1000000))
                ->setRooms($faker->numberBetween(1, 10))
                ->setSurface($faker->numberBetween(20, 350))
                ->setCity($faker->city)
                ->setAddress($faker->streetAddress)
                ->setPostalCode($faker->postcode)
                ->setIsSold(false)
                ->setDescription($faker->text(200))
            ;

            $manager->persist($property);
        }

        //create 2 user
        for ($i = 0; $i < 2; $i++) {
            $user = new User();
            $user
                ->setUsername($faker->userName)
                ->setPassword($this->passwordHasher->hashPassword(
                    $user,
                    'password'
                ))
                ->setRoles(['ROLE_USER'])
            ;
            $manager->persist($user);
        }
        $manager->flush();
    }
}
