<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ThemeSwitcherController extends AbstractController
{
    //make route for theme/light
    #[Route('/theme/light', name: 'app_theme_light')]
    public function light(Request $request): RedirectResponse
    {
        //create cookie
        $response = new Response();
        $response->headers->setCookie(
            new Cookie(
                'theme',
                'light',
                time() + (365 * 24 * 60 * 60)
            )
        );
        //send the response
        $response->send();

        //redirect to the last page
        return new RedirectResponse($request->headers->get('referer'));
    }

    //make route for theme/dark
    #[Route('/theme/dark', name: 'app_theme_dark')]
    public function dark(Request $request): RedirectResponse
    {
        //create cookie
        $response = new Response();
        $response->headers->setCookie(
            new Cookie(
                'theme',
                'dark',
                time() + (365 * 24 * 60 * 60)
            )
        );
        //send the response
        $response->send();

        //redirect to the last page
        return new RedirectResponse($request->headers->get('referer'));
    }
}
