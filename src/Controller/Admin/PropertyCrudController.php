<?php

namespace App\Controller\Admin;

use App\Entity\Heat;
use App\Entity\Property;
use App\Repository\HeatRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PropertyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Property::class;
    }

    public function __construct(private readonly HeatRepository $heatRepository)
    {
    }

    public function configureFields(string $pageName): iterable
    {

        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('title')->setColumns(12),
            TextField::new('city')->setColumns(3),
            TextField::new('address')->setColumns(6),
            TextField::new('postal_code')->setColumns(3),
            IntegerField::new('surface')->setColumns(3),
            IntegerField::new('rooms')->setColumns(3),
            IntegerField::new('bedrooms')->setColumns(3),
            IntegerField::new('floor')->setColumns(3),
            MoneyField::new('price')->setCurrency('EUR')->setColumns(4),
            CollectionField::new('heat')
                ->setEntryType(ChoiceType::class)
                ->setFormTypeOption('by_reference', false)
                ->setFormTypeOption('multiple', true)
                ->setFormTypeOption('choices', $this->heatRepository->findAll())
                ->setFormTypeOption('choice_label', fn (Heat $heat) => $heat->getName())
                ->hideOnForm(),
            AssociationField::new('heat')->onlyOnForms()->setColumns(4),
            ImageField::new('image')
                ->setUploadDir('public/uploads/properties')
                ->setBasePath('/uploads/properties')
                ->setColumns(4)
                ->setRequired(false)
                ->onlyWhenUpdating()
            ,
            ImageField::new('image')
            ->setColumns(4)
            ->setBasePath('/uploads/properties')
            ->setUploadDir('public/uploads/properties')
            ->setRequired(false)
            ,

            TextEditorField::new('description')->hideOnIndex()->setColumns(12),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}
