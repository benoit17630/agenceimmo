<?php

namespace App\Controller\Admin;

use App\Entity\Heat;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class HeatCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Heat::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
